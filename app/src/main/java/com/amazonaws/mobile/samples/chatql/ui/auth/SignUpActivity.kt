package com.amazonaws.mobile.samples.chatql.ui.auth

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.TextView
import com.amazonaws.mobile.samples.chatql.R
import com.amazonaws.mobile.samples.chatql.afterTextChanged
import com.amazonaws.mobile.samples.chatql.repositories.IdentityRequest
import com.amazonaws.mobile.samples.chatql.services.AnalyticsService
import com.amazonaws.mobile.samples.chatql.services.AuthenticationType
import com.amazonaws.mobile.samples.chatql.validate
import com.amazonaws.mobile.samples.chatql.viewmodels.AuthenticatorViewModel
import kotlinx.android.synthetic.main.cancel_button.*
import kotlinx.android.synthetic.main.signup_form.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class SignUpActivity : AppCompatActivity() {
    private val model by viewModel<AuthenticatorViewModel>()
    private val analytics by inject<AnalyticsService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        // Wire up the cancel button
        cancel_button.onClick { this@SignUpActivity.finish() }

        // Wire up the username field
        with (sign_up_username_field) {
            validate(getString(R.string.valid_username)) { Validators.username(it) }
            afterTextChanged { checkSubmitEnabled() }
        }

        // Wire up the password field
        with (sign_up_password_field) {
            validate(getString(R.string.valid_password)) { Validators.password(it) }
            afterTextChanged { checkSubmitEnabled() }
        }

        // Wire up the full name field
        with (sign_up_name_field) {
            validate(getString(R.string.valid_fullName)) { Validators.fullName(it) }
            afterTextChanged { checkSubmitEnabled() }
        }

        // Wire up the phone number field
        with (sign_up_phone_field) {
            validate(getString(R.string.valid_phoneNumber)) { Validators.phoneNumber(it) }
            afterTextChanged { checkSubmitEnabled() }
        }

        // Wire up the sign-in button
        sign_up_button.onClick { handleSignUp() }

        checkSubmitEnabled()
    }

    /**
     * Checks the form fields to see if the submit button should be enabled
     */
    private fun checkSubmitEnabled() {
        sign_up_button.isEnabled =
                Validators.username(sign_up_username_field.text.toString()) &&
                Validators.password(sign_up_password_field.text.toString()) &&
                Validators.fullName(sign_up_name_field.text.toString()) &&
                Validators.phoneNumber(sign_up_phone_field.text.toString())
    }

    /**
     * Handle the form submission
     */
    @SuppressLint("InflateParams")
    private fun handleSignUp() {
        model.initiateSignup { identityRequest, params, callback ->
            when (identityRequest) {
                IdentityRequest.NEED_SIGNUP -> {
                    callback(mapOf(
                            "username" to sign_up_username_field.text.toString(),
                            "password" to sign_up_password_field.text.toString(),
                            "phone" to sign_up_phone_field.text.toString(),
                            "name" to sign_up_name_field.text.toString()
                    ))
                }

                IdentityRequest.NEED_MULTIFACTORCODE -> {
                    val mfaDialog = layoutInflater.inflate(R.layout.dialog_multifactor_auth, null)
                    val mfaCodeInput = mfaDialog.find(R.id.dialog_mfa_code) as EditText
                    val mfaInstructions = mfaDialog.find(R.id.dialog_mfa_instructions) as TextView
                    params?.let {
                        val deliveryTo = it.getOrDefault("deliveryTo", "UNKNOWN")
                        mfaInstructions.text = getString(R.string.specific_mfa_instructions, deliveryTo)
                    }
                    alert {
                        title = "Enter Secondary Code"
                        customView = mfaDialog
                        positiveButton("OK") { callback(mapOf("mfaCode" to mfaCodeInput.text.toString())) }
                    }.show()
                }

                IdentityRequest.SUCCESS -> {
                    toast("Sign up Successful")
                    analytics.recordAuthenticationEvent(AuthenticationType.SIGN_UP, true, sign_up_username_field.text.toString())
                    this@SignUpActivity.finish()
                }

                IdentityRequest.FAILURE -> {
                    analytics.recordAuthenticationEvent(AuthenticationType.SIGN_UP, false, sign_up_username_field.text.toString())
                    alert(params?.get("message") ?: "Error submitting new credentials") {
                        title = "Sign Up Failed"
                        positiveButton("Close") { /* Do Nothing */ }
                    }.show()
                }

                else -> {
                    analytics.recordEvent("_userauth.error", mapOf("error" to "Unknown identity request $identityRequest"))
                    alert("Unknown or unexpected identity request") {
                        title = "Something went wrong!"
                        positiveButton("Close") { this@SignUpActivity.finish() }
                    }.show()
                }
            }
        }
    }

}