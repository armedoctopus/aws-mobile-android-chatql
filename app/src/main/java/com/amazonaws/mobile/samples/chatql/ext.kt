package com.amazonaws.mobile.samples.chatql

import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.EditText

/**
 * Replace the entire contents of a CharSequence with something else
 * @param s [String] the replacement string
 */
fun CharSequence.replace(s: String) {
    replaceRange(indices, s)
}

/**
 * Returns true if the string is a valid email address
 */
fun String.isValidEmail(): Boolean = this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

/**
 * Get the content view of the current activity.
 */
inline val AppCompatActivity.contentView
    get() = this.findViewById<View>(android.R.id.content)

/**
 * Define a callback for when a text field changes
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
    })
}

/**
 * Validate the contents of the EditText control
 */
fun EditText.validate(message: String, validator: (String) -> Boolean) {
    this.afterTextChanged { this.error = if (validator(it)) null else message }
    this.error = if(validator(this.text.toString())) null else message
}


/**
 * Default TAG for a class
 */
val <T : Any> T.TAG
    get() = this::class.simpleName
