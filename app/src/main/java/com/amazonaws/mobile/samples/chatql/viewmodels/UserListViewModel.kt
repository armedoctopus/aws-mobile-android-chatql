package com.amazonaws.mobile.samples.chatql.viewmodels

import android.arch.lifecycle.ViewModel
import com.amazonaws.mobile.samples.chatql.repositories.UsersRepository

class UserListViewModel(private val usersRepository: UsersRepository) : ViewModel() {
    /**
     * The current list of users, as an array of User objects
     */
    val users = usersRepository.users

    /**
     * Observable work list that emits adds and deletes as they happen
     */
    val userOperations = usersRepository.userOperations

}