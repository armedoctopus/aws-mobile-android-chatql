package com.amazonaws.mobile.samples.chatql.ui.auth

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import com.amazonaws.mobile.samples.chatql.*
import com.amazonaws.mobile.samples.chatql.repositories.IdentityRequest
import com.amazonaws.mobile.samples.chatql.models.TokenType
import com.amazonaws.mobile.samples.chatql.services.AnalyticsService
import com.amazonaws.mobile.samples.chatql.services.AuthenticationType
import com.amazonaws.mobile.samples.chatql.ui.MainActivity
import com.amazonaws.mobile.samples.chatql.viewmodels.AuthenticatorViewModel
import kotlinx.android.synthetic.main.signin_form.*
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class SignInActivity : AppCompatActivity() {
    private val model by viewModel<AuthenticatorViewModel>()
    private val analytics by inject<AnalyticsService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        // Wire up the username field
        with (sign_in_username_field) {
            validate(getString(R.string.valid_username)) { Validators.username(it) }
            afterTextChanged { checkLoginEnabled() }
        }

        // Wire up the password field
        with (sign_in_password_field) {
            validate(getString(R.string.valid_password)) { Validators.password(it) }
            afterTextChanged { checkLoginEnabled() }
        }

        // Wire up the sign-in button
        sign_in_button.onClick { handleSignIn() }

        // Wire up the forgot-password and sign-up buttons
        initiate_forgot_password_button.onClick {
            startActivity<ForgotPasswordActivity>("username" to sign_in_username_field.text.toString())
        }
        initiate_sign_up_button.onClick {
            startActivity<SignUpActivity>()
        }

        checkLoginEnabled()
    }

    override fun onResume() {
        super.onResume()

        if (sign_in_username_field.text.toString().isBlank() && model.storedUsername.value != null) {
            sign_in_username_field.text.replace(model.storedUsername.value!!)
            sign_in_password_field.requestFocus()
        }
        checkLoginEnabled()
    }

    /**
     * Check to see if the login button should be enabled
     */
    private fun checkLoginEnabled() {
        sign_in_button.isEnabled =
                Validators.username(sign_in_username_field.text.toString()) &&
                Validators.password(sign_in_password_field.text.toString())
    }

    /**
     * Handle the sign-in button click
     */
    @SuppressLint("InflateParams")
    private fun handleSignIn() {
        model.initiateSignin { identityRequest, params, callback ->
            when (identityRequest) {
                IdentityRequest.NEED_CREDENTIALS -> {
                    callback(mapOf(
                            "username" to sign_in_username_field.text.toString(),
                            "password" to sign_in_password_field.text.toString()
                    ))
                }

                IdentityRequest.NEED_MULTIFACTORCODE -> {
                    val mfaDialog = layoutInflater.inflate(R.layout.dialog_multifactor_auth, null)
                    val mfaCodeInput = mfaDialog.find(R.id.dialog_mfa_code) as EditText
                    val mfaInstructions = mfaDialog.find(R.id.dialog_mfa_instructions) as TextView
                    params?.let {
                        val deliveryTo = it.getOrDefault("deliveryTo", "UNKNOWN")
                        mfaInstructions.text = getString(R.string.specific_mfa_instructions, deliveryTo)
                    }
                    alert {
                        title = "Enter Secondary Code"
                        customView = mfaDialog
                        positiveButton("OK") { callback(mapOf("mfaCode" to mfaCodeInput.text.toString())) }
                    }.show()
                }

                IdentityRequest.NEED_NEWPASSWORD -> {
                    val newPasswordDialog = layoutInflater.inflate(R.layout.dialog_new_password, null)
                    val passwordInput = newPasswordDialog.find(R.id.new_password_form_password) as EditText
                    alert {
                        title = "Enter New Password"
                        customView = newPasswordDialog
                        positiveButton("OK") { callback(mapOf("password" to passwordInput.text.toString())) }
                    }.show()
                }

                IdentityRequest.SUCCESS -> {
                    analytics.recordAuthenticationEvent(AuthenticationType.SIGN_IN, true, sign_in_username_field.text.toString())
                    Log.d(TAG, "accessToken: ${model.currentUser.value!!.tokens[TokenType.ACCESS_TOKEN]}")
                    startActivity(intentFor<MainActivity>().singleTop())
                }

                IdentityRequest.FAILURE -> {
                    analytics.recordAuthenticationEvent(AuthenticationType.SIGN_IN, false, sign_in_username_field.text.toString())
                    alert(params?.get("message") ?: "Error submitting credentials") {
                        title = "Login Denied"
                        positiveButton("Close") { /* Do Nothing */ }
                    }.show()
                }

                else -> {
                    analytics.recordEvent("_userauth.error", mapOf("error" to "Unknown identity request $identityRequest"))
                    alert("Unknown or unexpected identity request") {
                        title = "Something went wrong!"
                        positiveButton("Close") { /* Do Nothing */ }
                    }.show()
                }

            }
        }
    }

}