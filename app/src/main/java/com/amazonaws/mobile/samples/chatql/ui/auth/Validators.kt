package com.amazonaws.mobile.samples.chatql.ui.auth

import com.amazonaws.mobile.samples.chatql.isValidEmail

class Validators {
    companion object {
        /**
         * Validator for the username
         */
        fun username(s: String): Boolean = s.isNotEmpty() && s.isValidEmail()

        /**
         * Validator for the password
         */
        fun password(s: String): Boolean = s.isNotEmpty() && s.trim().length >= 6 && s.trim().length <= 48

        /**
         * Validator for the full name
         */
        fun fullName(s: String): Boolean = s.isNotEmpty() && s.isNotBlank() && s.trim().length > 3

        /**
         * Validator for the phone number
         */
        fun phoneNumber(s: String): Boolean = s.isNotBlank()
    }
}