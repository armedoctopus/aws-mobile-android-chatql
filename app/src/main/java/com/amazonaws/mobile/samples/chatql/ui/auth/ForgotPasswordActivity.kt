package com.amazonaws.mobile.samples.chatql.ui.auth

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.TextView
import com.amazonaws.mobile.samples.chatql.R
import com.amazonaws.mobile.samples.chatql.afterTextChanged
import com.amazonaws.mobile.samples.chatql.replace
import com.amazonaws.mobile.samples.chatql.repositories.IdentityRequest
import com.amazonaws.mobile.samples.chatql.services.AnalyticsService
import com.amazonaws.mobile.samples.chatql.services.AuthenticationType
import com.amazonaws.mobile.samples.chatql.validate
import com.amazonaws.mobile.samples.chatql.viewmodels.AuthenticatorViewModel
import kotlinx.android.synthetic.main.cancel_button.*
import kotlinx.android.synthetic.main.forgot_password_form.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class ForgotPasswordActivity : AppCompatActivity() {
    private val model by viewModel<AuthenticatorViewModel>()
    private val analytics by inject<AnalyticsService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        if (intent.hasExtra("username")) {
            with(auth_fp_username_field) {
                text.replace(intent.getStringExtra("username"))
                if (text.isNotBlank()) auth_fp_password_field.requestFocus()
            }
        }

        cancel_button.onClick { this@ForgotPasswordActivity.finish() }

        with(auth_fp_username_field) {
            validate(getString(R.string.valid_username)) { Validators.username(it) }
            afterTextChanged { checkSubmitEnabled() }
        }

        with(auth_fp_password_field) {
            validate(getString(R.string.valid_password)) { Validators.password(it) }
            afterTextChanged { checkSubmitEnabled() }
        }

        auth_fp_button.onClick { handleSubmit() }

        checkSubmitEnabled()
    }

    private fun checkSubmitEnabled() {
        auth_fp_button.isEnabled =
                Validators.username(auth_fp_username_field.text.toString()) &&
                Validators.password(auth_fp_password_field.text.toString())
    }

    @SuppressLint("InflateParams")
    private fun handleSubmit() {
        model.initiateForgotPassword { identityRequest, params, callback ->
            when (identityRequest) {
                IdentityRequest.NEED_CREDENTIALS -> {
                    callback(mapOf(
                            "username" to auth_fp_username_field.text.toString(),
                            "password" to auth_fp_password_field.text.toString()
                    ))
                }

                IdentityRequest.NEED_MULTIFACTORCODE -> {
                    val mfaDialog = layoutInflater.inflate(R.layout.dialog_multifactor_auth, null)
                    val mfaCodeInput = mfaDialog.find(R.id.dialog_mfa_code) as EditText
                    val mfaInstructions = mfaDialog.find(R.id.dialog_mfa_instructions) as TextView
                    params?.let {
                        val deliveryTo = it.getOrDefault("deliveryTo", "UNKNOWN")
                        mfaInstructions.text = getString(R.string.specific_mfa_instructions, deliveryTo)
                    }
                    alert {
                        title = "Enter Secondary Code"
                        customView = mfaDialog
                        positiveButton("OK") { callback(mapOf("mfaCode" to mfaCodeInput.text.toString())) }
                    }.show()
                }

                IdentityRequest.SUCCESS -> {
                    analytics.recordAuthenticationEvent(AuthenticationType.PASSWORD_RESET, true, auth_fp_username_field.text.toString())
                    this@ForgotPasswordActivity.finish()
                }

                IdentityRequest.FAILURE -> {
                    analytics.recordAuthenticationEvent(AuthenticationType.PASSWORD_RESET, false, auth_fp_username_field.text.toString())
                    alert(params?.get("message") ?: "Error submitting new credentials") {
                        title = "Password Reset Failed"
                        positiveButton("Close") { /* Do nothing */ }
                    }.show()
                }

                else -> {
                    analytics.recordEvent("_userauth.error", mapOf("error" to "Unknown identity request $identityRequest"))
                    alert("Unknown or unexpected identity request") {
                        title = "Something went wrong!"
                        positiveButton("Close") { /* Do Nothing */ }
                    }.show()
                }
            }
        }
    }
}