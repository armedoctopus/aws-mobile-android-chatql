package com.amazonaws.mobile.samples.chatql

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log
import com.amazonaws.mobile.samples.chatql.viewmodels.AuthenticatorViewModel
import com.amazonaws.mobile.samples.chatql.repositories.IdentityRepository
import com.amazonaws.mobile.samples.chatql.repositories.UsersRepository
import com.amazonaws.mobile.samples.chatql.repositories.aws.AWSIdentityRepository
import com.amazonaws.mobile.samples.chatql.repositories.aws.AWSUsersRepository
import com.amazonaws.mobile.samples.chatql.services.AnalyticsService
import com.amazonaws.mobile.samples.chatql.services.aws.AWSService
import com.amazonaws.mobile.samples.chatql.services.local.LocalAnalyticsService
import com.amazonaws.mobile.samples.chatql.viewmodels.UserListViewModel
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.log.EmptyLogger

/**
 * This class is the entry-point into the application and is used for maintaining global
 * application state.  Its primary use is to set up dependency injection.
 */
@SuppressWarnings("unused")
class MainApp : Application() {
    companion object {
        /**
         * List of service modules - each service is a singleton that can be swapped out to
         * connect to a different API.  For example, you may define a "DataService" as an
         * interface and then have a concrete implementation that connects to AWS.  During
         * testing, you would provide a local version of the data service.
         */
        private val servicesModule = module {
            single { AWSService(get()) }
            single { LocalAnalyticsService() as AnalyticsService }
        }

        /**
         * List of repositories.
         */
        val repositoriesModule = module {
            single { AWSIdentityRepository(get(), get()) as IdentityRepository }
            single { AWSUsersRepository(get(), get()) as UsersRepository }
        }

        /**
         * List of view models
         */
        val viewModelsModule = module {
            viewModel { AuthenticatorViewModel(get()) }
            viewModel { UserListViewModel(get()) }
        }
    }

    override fun onCreate() {
        super.onCreate()

        // Handle logging of the activity lifecycle
        if (BuildConfig.DEBUG) {
            registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                override fun onActivityPaused(p0: Activity?) {
                    p0?.let { activity -> Log.d(TAG, "PAUSE ${activity.localClassName}") }
                }

                override fun onActivityResumed(p0: Activity?) {
                    p0?.let { activity -> Log.d(TAG, "RESUME ${activity.localClassName}") }
                }

                override fun onActivityStarted(p0: Activity?) {
                    p0?.let { activity -> Log.d(TAG, "START ${activity.localClassName}") }
                }

                override fun onActivityDestroyed(p0: Activity?) {
                    p0?.let { activity -> Log.d(TAG, "DESTROY ${activity.localClassName}") }
                }

                override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
                    p0?.let { activity -> Log.d(TAG, "SAVE ${activity.localClassName}") }
                }

                override fun onActivityStopped(p0: Activity?) {
                    p0?.let { activity -> Log.d(TAG, "STOP ${activity.localClassName}") }
                }

                override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
                    p0?.let { activity -> Log.d(TAG, "CREATE ${activity.localClassName}") }
                }
            })
        }

        // Initialize Koin dependency injection
        val appModules = listOf(servicesModule, repositoriesModule, viewModelsModule)
        val logger = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()
        startKoin(this, appModules, logger = logger)
    }
}