package com.amazonaws.mobile.samples.chatql.repositories

import android.arch.lifecycle.LiveData
import com.amazonaws.mobile.samples.chatql.models.AuthUser

enum class IdentityRequest {
    NEED_SIGNUP,
    NEED_CREDENTIALS,
    NEED_NEWPASSWORD,
    NEED_MULTIFACTORCODE,
    SUCCESS,
    FAILURE
}

typealias IdentityResponse = (Map<String, String>?) -> Unit
typealias IdentityHandler = (IdentityRequest, Map<String, String>?, IdentityResponse) -> Unit

interface IdentityRepository {
    /**
     * Property for the current user record (null if not signed in)
     */
    val currentUser: LiveData<AuthUser?>

    /**
     * Stored username
     */
    val storedUsername: LiveData<String?>

    /**
     * Initiate a sign-in flow
     */
    fun initiateSignin(handler: IdentityHandler)

    /**
     * Initiate sign-up flow
     */
    fun initiateSignup(handler: IdentityHandler)

    /**
     * Initiate a forgot password flow
     */
    fun initiateForgotPassword(handler: IdentityHandler)

    /**
     * Initiate a sign-out flow
     */
    fun signOut()
}