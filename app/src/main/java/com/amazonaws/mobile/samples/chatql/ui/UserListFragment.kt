package com.amazonaws.mobile.samples.chatql.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import com.amazonaws.mobile.samples.chatql.R
import com.amazonaws.mobile.samples.chatql.models.User
import com.amazonaws.mobile.samples.chatql.models.UserOperationType
import com.amazonaws.mobile.samples.chatql.services.AnalyticsService
import com.amazonaws.mobile.samples.chatql.viewmodels.UserListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.user_list_fragment.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class UserListFragment : Fragment() {
    private val model by viewModel<UserListViewModel>()
    private val analytics by inject<AnalyticsService>()

    // Current list of users
    private val userList: ArrayList<User> = ArrayList()

    // Disposable for subscriptions
    private val compositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the view for use
        val view = inflater.inflate(R.layout.user_list_fragment, container, false)

        // Initialize the user list
        initializeUserList(view.findViewById(R.id.user_list_view))

        // Return the view
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    /**
     * Initializes the user list management piece
     */
    private fun initializeUserList(listView: ListView) {
        /* Pre-populate the user list with whatever we have */
        userList.addAll(model.users.toList())

        /* Create an adapter for the list view */
        val adapter = StableArrayAdapter(this.context!!, android.R.layout.simple_list_item_1, userList)

        /* Subscribe to the changes */
        val disposable = model.userOperations
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { operation ->
                    analytics.recordEvent("USER_LIST", mapOf("operation" to operation.operationType.toString(), "userId" to operation.user.userId, "name" to operation.user.name))
                    when (operation.operationType) {
                        UserOperationType.ADD -> {
                            adapter.add(operation.user)
                        }

                        UserOperationType.DELETE -> {
                            adapter.remove(operation.user)
                        }

                        UserOperationType.MODIFY -> {
                            adapter.replace(operation.user)
                        }
                    }
                }
        compositeDisposable.add(disposable)

        /* Add the adapter to the list view */
        listView.adapter = adapter
    }

    /**
     * The adapter class for the users list
     */
    private class StableArrayAdapter(context: Context, resource: Int, objects: MutableList<User>) : ArrayAdapter<User>(context, resource, objects) {
        val mIdMap = HashMap<User,Long>()
        var lastId = 0L

        init {
            // Generate a positional integer so that we have a good stable ID
            objects.forEach { item -> mIdMap[item] = lastId++ }
        }

        /**
         * Get the Row ID associated with the specified position in the data set
         */
        override fun getItemId(position: Int): Long = mIdMap.getOrDefault(getItem(position)!!, -1L)

        /**
         * Indicates whether the item IDs are stable across changes to the underlying data
         */
        override fun hasStableIds(): Boolean = true

        /**
         * Add a new item to the list
         */
        override fun add(user: User) {
            super.add(user)
            mIdMap[user] = lastId++
        }

        /**
         * Remove an item from the list
         */
        override fun remove(user: User) {
            super.remove(user)
            mIdMap.remove(user)
        }

        /**
         * Replace a record in the list
         */
        fun replace(user: User) {
            // Find the old user that matches here
            var oldUser: User? = null
            mIdMap.forEach { u,_ -> if (user.userId == u.userId) oldUser = u }

            // Read the IdMap, then remove the old one and add the new one
            oldUser?.run {
                val id = mIdMap[this]!! // This is ok because we've already determined its ok
                mIdMap.remove(this)
                mIdMap[user] = id
                this@StableArrayAdapter.notifyDataSetChanged()
            }
        }
    }
}