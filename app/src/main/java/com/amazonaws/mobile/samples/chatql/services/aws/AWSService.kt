package com.amazonaws.mobile.samples.chatql.services.aws

import android.content.Context
import com.amazonaws.mobile.auth.core.IdentityManager
import com.amazonaws.mobile.auth.userpools.CognitoUserPoolsSignInProvider
import com.amazonaws.mobile.config.AWSConfiguration

/**
 * Access to the configuration and identity pools provider (Identitymanager) for the
 * connection to AWS.
 */
class AWSService(context: Context) {
    val configuration = AWSConfiguration(context)
    val identityManager = IdentityManager(context, configuration)
    val credentialsProvider = identityManager.credentialsProvider

    init {
        identityManager.addSignInProvider(CognitoUserPoolsSignInProvider::class.java)
        IdentityManager.setDefaultIdentityManager(identityManager)
    }
}