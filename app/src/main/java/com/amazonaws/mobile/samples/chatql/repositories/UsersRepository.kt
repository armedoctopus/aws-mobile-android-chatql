package com.amazonaws.mobile.samples.chatql.repositories

import com.amazonaws.mobile.samples.chatql.models.User
import com.amazonaws.mobile.samples.chatql.models.UserOperation
import io.reactivex.Observable

interface UsersRepository {
    /**
     * The current list of users, as an array of User objects
     */
    val users: Array<User>

    /**
     * Observable work list that emits adds and deletes as they happen
     */
    val userOperations: Observable<UserOperation>
}