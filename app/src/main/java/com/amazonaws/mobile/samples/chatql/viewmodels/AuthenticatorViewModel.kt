package com.amazonaws.mobile.samples.chatql.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.amazonaws.mobile.samples.chatql.repositories.IdentityHandler
import com.amazonaws.mobile.samples.chatql.repositories.IdentityRepository
import com.amazonaws.mobile.samples.chatql.models.AuthUser

class AuthenticatorViewModel(private val identityRepository: IdentityRepository) : ViewModel() {
    /**
     * Property for the current user record (null if not signed in)
     */
    val currentUser: LiveData<AuthUser?> = identityRepository.currentUser

    /**
     * Stored username
     */
    val storedUsername: LiveData<String?> = identityRepository.storedUsername

    /**
     * Initiate a sign-in flow
     */
    fun initiateSignin(handler: IdentityHandler) = identityRepository.initiateSignin(handler)

    /**
     * Initiate sign-up flow
     */
    fun initiateSignup(handler: IdentityHandler) = identityRepository.initiateSignup(handler)

    /**
     * Initiate a forgot password flow
     */
    fun initiateForgotPassword(handler: IdentityHandler) = identityRepository.initiateForgotPassword(handler)

    /**
     * Initiate a sign-out flow
     */
    fun signOut() = identityRepository.signOut()
}