package com.amazonaws.mobile.samples.chatql.models

data class User(val userId: String, var name: String)