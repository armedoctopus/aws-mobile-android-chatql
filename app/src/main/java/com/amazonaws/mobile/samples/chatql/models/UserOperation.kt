package com.amazonaws.mobile.samples.chatql.models

enum class UserOperationType {
    ADD,
    DELETE,
    MODIFY
}

data class UserOperation(val operationType: UserOperationType, val user: User)