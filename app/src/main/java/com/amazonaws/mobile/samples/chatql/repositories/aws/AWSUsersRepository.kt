package com.amazonaws.mobile.samples.chatql.repositories.aws

import android.content.Context
import com.amazonaws.mobile.samples.chatql.models.User
import com.amazonaws.mobile.samples.chatql.models.UserOperation
import com.amazonaws.mobile.samples.chatql.models.UserOperationType
import com.amazonaws.mobile.samples.chatql.repositories.UsersRepository
import com.amazonaws.mobile.samples.chatql.services.aws.AWSService
import io.reactivex.Observable
import java.util.*
import kotlin.concurrent.thread

class AWSUsersRepository(private val context: Context, private val awsService: AWSService) : UsersRepository {
    /**
     * Internal list of users
     */
    private val userList: ArrayList<User> = ArrayList()

    /**
     * The current list of users, as an array of User objects
     */
    override val users: Array<User>
        get() = userList.toTypedArray()

    /**
     * Observable work list that emits adds and deletes as they happen
     */
    override val userOperations: Observable<UserOperation>

    /**
     * Initialize the userOperations queue
     */
    init {
        userOperations = Observable.create { subscriber ->
            thread(start = true) {
                var id = 1

                while(true) {
                    Thread.sleep(5000L)
                    val user = User(UUID.randomUUID().toString(), "User ${id++}")
                    userList.add(user)
                    subscriber.onNext(UserOperation(UserOperationType.ADD, user))
                }
            }
        }
    }
}