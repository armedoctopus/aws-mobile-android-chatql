package com.amazonaws.mobile.samples.chatql.services.local

import android.util.Log
import com.amazonaws.mobile.samples.chatql.TAG
import com.amazonaws.mobile.samples.chatql.services.AnalyticsService
import com.amazonaws.mobile.samples.chatql.services.AuthenticationType

class LocalAnalyticsService : AnalyticsService {
    init {
        Log.d(TAG, "START-SESSION")
    }
    /**
     * Record a custom event into the analytics stream
     *
     * @param name the custom event name
     * @param [attributes] a list of key-value pairs for recording string attributes
     * @param [metrics] a list of key-value pairs for recording numeric metrics
     */
    override fun recordEvent(name: String, attributes: Map<String, String>?, metrics: Map<String, Double>?) {
        val event = StringBuilder("")
        for ((k,v) in attributes.orEmpty()) {
            event.append("$k='$v', ")
        }
        for ((k,v) in metrics.orEmpty()) {
            event.append(String.format("%s=%.2f, ", k, v))
        }
        Log.d(TAG, "$name: ${event.substring(0, event.length - 2)}")
    }

    /**
     * Record an authentication record
     *
     * @param authType the authentication type
     * @param isSuccessful true if the authentication was successful
     * @param username the username that was used
     */
    override fun recordAuthenticationEvent(authType: AuthenticationType, isSuccessful: Boolean, username: String) {
        recordEvent(authType.name, mapOf(
                "success" to isSuccessful.toString(),
                "username" to username
        ))
    }
}