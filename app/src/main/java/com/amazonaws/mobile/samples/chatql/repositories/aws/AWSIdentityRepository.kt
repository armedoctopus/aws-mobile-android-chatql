package com.amazonaws.mobile.samples.chatql.repositories.aws

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread
import com.amazonaws.mobile.samples.chatql.models.TokenType
import com.amazonaws.mobile.samples.chatql.models.AuthUser
import com.amazonaws.mobile.samples.chatql.repositories.*
import com.amazonaws.mobile.samples.chatql.services.aws.AWSService
import com.amazonaws.mobileconnectors.cognitoidentityprovider.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.*
import java.lang.Exception
import java.lang.RuntimeException

class AWSIdentityRepository(context: Context, service: AWSService) : IdentityRepository {
    companion object {
        private val DO_NOTHING: IdentityResponse = { /* Do Nothing */ }
        private val PREFS_FILE: String = "${this::class.java.canonicalName}.PREFS"
        private const val USERNAME_PREF: String = "username"
    }

    private val mCurrentUser: MutableLiveData<AuthUser?> = MutableLiveData()
    private val mStoredUsername: MutableLiveData<String?> = MutableLiveData()
    private val sharedPreferences = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
    private val userPool: CognitoUserPool = CognitoUserPool(context, service.configuration)

    init {
        userPool.currentUser.getSessionInBackground(object : AuthenticationHandler {
            /**
             * This method is called to deliver valid tokens, when valid tokens were locally
             * available (cached) or after successful completion of the authentication process.
             * The `newDevice` will is an instance of [CognitoDevice] for this device, and this
             * parameter will be not null during these cases:
             * 1- If the user pool allows devices to be remembered and this is is a new device, that is
             * first time authentication on this device.
             * 2- When the cached device key is lost and, hence, the service identifies this as a new device.
             *
             * @param userSession               Contains valid user tokens.
             * @param newDevice                 [CognitoDevice], will be null if this is not a new device.
             */
            override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                storeCurrentUser(userSession) { _,_,_ ->
                }
            }

            override fun onFailure(exception: Exception?) { }
            override fun getAuthenticationDetails(authenticationContinuation: AuthenticationContinuation?, userId: String?) { }
            override fun authenticationChallenge(continuation: ChallengeContinuation?) { }
            override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) { }
        })

        // Load the currently stored username from the shared preference
        mStoredUsername.postValue(sharedPreferences.getString(USERNAME_PREF, null))
    }

    /**
     * Store the current user based on the user session
     */
    private fun storeCurrentUser(userSession: CognitoUserSession?, handler : IdentityHandler) {
        if (userSession != null) {
            val user = AuthUser().apply {
                username = userSession.username
                tokens[TokenType.ACCESS_TOKEN] = userSession.accessToken.jwtToken
                tokens[TokenType.ID_TOKEN] = userSession.idToken.jwtToken
                tokens[TokenType.REFRESH_TOKEN] = userSession.refreshToken.token
            }
            userPool.currentUser.getDetailsInBackground(object : GetDetailsHandler {
                override fun onSuccess(cognitoUserDetails: CognitoUserDetails?) {
                    if (cognitoUserDetails != null) {
                        for (entry in cognitoUserDetails.attributes.attributes)
                            user.userAttributes[entry.key] = entry.value
                    }
                    runOnUiThread {
                        mCurrentUser.postValue(user)
                        handler(IdentityRequest.SUCCESS, null, DO_NOTHING)
                    }
                }

                override fun onFailure(exception: Exception?) = handleFailure(handler, exception?.localizedMessage)
            })
        } else {
            mCurrentUser.postValue(null)
        }
    }

    /**
     * We need to handle failures all over the place - this helps us do it consistently
     */
    private fun handleFailure(handler: IdentityHandler, message: String?) {
        runOnUiThread {
            handler(IdentityRequest.FAILURE, mapOf("message" to (message ?: "Unknown error")), DO_NOTHING)
        }
    }

    /**
     * Update the username that is stored in the shared preferences
     */
    fun updateStoredUsername(username: String?) {
        with (sharedPreferences.edit()) {
            if (username == null)
                remove(USERNAME_PREF)
            else
                putString(USERNAME_PREF, username)
            apply()
        }
        mStoredUsername.postValue(username)
    }

    /**
     * Convert an email address (our username) to the username form.
     */
    private fun toUsername(email: String): String = email.replace('@','_').replace('.','_')

    /**
     * Property for the current user record (null if not signed in)
     */
    override val currentUser: LiveData<AuthUser?> = mCurrentUser

    /**
     * Stored username
     */
    override val storedUsername: LiveData<String?> = mStoredUsername

    /**
     * Initiate a sign-in flow
     */
    override fun initiateSignin(handler: IdentityHandler) {
        var email: String? = null

        try {
            userPool.currentUser.getSessionInBackground(object : AuthenticationHandler {
                /**
                 * This method is called to deliver valid tokens, when valid tokens were locally
                 * available (cached) or after successful completion of the authentication process.
                 * The `newDevice` will is an instance of [CognitoDevice] for this device, and this
                 * parameter will be not null during these cases:
                 * 1- If the user pool allows devices to be remembered and this is is a new device, that is
                 * first time authentication on this device.
                 * 2- When the cached device key is lost and, hence, the service identifies this as a new device.
                 *
                 * @param userSession               Contains valid user tokens.
                 * @param newDevice                 [CognitoDevice], will be null if this is not a new device.
                 */
                override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                    if (email != null) updateStoredUsername(email)
                    storeCurrentUser(userSession, handler)
                }

                /**
                 * This method is called when a fatal exception was encountered during
                 * authentication. The current authentication process continue because of the error
                 * , hence a continuation is not available. Probe `exception` for details.
                 *
                 * @param exception is this Exception leading to authentication failure.
                 */
                override fun onFailure(exception: Exception?) = handleFailure(handler, exception?.localizedMessage)

                /**
                 * Call out to the dev to get the credentials for a user.
                 *
                 * @param authenticationContinuation is a [AuthenticationContinuation] object that should
                 * be used to continue with the authentication process when
                 * the users' authentication details are available.
                 * @param userId                     Is the user-ID (username  or alias) used in authentication.
                 * This will be null if the user ID is not available.
                 */
                override fun getAuthenticationDetails(authenticationContinuation: AuthenticationContinuation?, userId: String?) {
                    val continuation = checkNotNull(authenticationContinuation) { "Invalid Continuation Handler" }
                    runOnUiThread {
                        handler(IdentityRequest.NEED_CREDENTIALS, null) { r ->
                            run {
                                val response = checkNotNull(r) { "Invalid identity response" }
                                email = response["username"]?.trim()
                                val username = toUsername(email ?: "")
                                val password = response["password"]?.trim() ?: ""
                                continuation.setAuthenticationDetails(AuthenticationDetails(username, password, null))
                                continuation.continueTask()
                            }
                        }
                    }
                }

                /**
                 * Call out to the dev to respond to a challenge.
                 * The authentication process as presented the user with the a challenge, to successfully authenticate.
                 * This a generic challenge, that is not MFA or user password verification.
                 *
                 * @param challengeContinuation contains details about the challenge and allows dev to respond to the
                 * challenge.
                 */
                override fun authenticationChallenge(challengeContinuation: ChallengeContinuation?) {
                    val continuation = checkNotNull(challengeContinuation) { "Invalid Continuation Handler" }
                    when (continuation.challengeName) {
                        "NEW_PASSWORD_REQUIRED" -> {
                            runOnUiThread {
                                handler(IdentityRequest.NEED_NEWPASSWORD, null) { r ->
                                    run {
                                        val response = checkNotNull(r) { "Invalid new password response" }
                                        continuation.parameters["NEW_PASSWORD"] = response["password"] ?: ""
                                        continuation.continueTask()
                                    }
                                }
                            }
                        }
                        else -> {
                            throw RuntimeException("Unknown challenge name: ${continuation.challengeName}")
                        }
                    }
                }

                /**
                 * Call out to the dev to send MFA code.
                 * MFA code would have been sent via the deliveryMethod before this is invoked.
                 * This callback can be invoked in two scenarios -
                 * 1)  MFA verification is required and only one possible MFA delivery medium is
                 * available.
                 * 2)  MFA verification is required and a MFA delivery medium was successfully set.
                 * 3)  An MFA code sent earlier was incorrect and at-least one more attempt to send
                 * MFA code is available.
                 *
                 * @param mfaContinuation medium through which the MFA will be delivered
                 */
                override fun getMFACode(mfaContinuation: MultiFactorAuthenticationContinuation?) {
                    val continuation = checkNotNull(mfaContinuation) { "Invalid MFA continuation" }
                    runOnUiThread {
                        handler(IdentityRequest.NEED_MULTIFACTORCODE, null) { r ->
                            run {
                                val response = checkNotNull(r) { "Invalid MFA response" }
                                continuation.setMfaCode(response["mfaCode"] ?: "")
                                continuation.continueTask()
                            }
                        }
                    }
                }
            })
        } catch (exception: Exception) {
            handleFailure(handler, exception.localizedMessage)
        }
    }

    /**
     * Initiate sign-up flow
     */
    override fun initiateSignup(handler: IdentityHandler) {
        try {
            handler(IdentityRequest.NEED_SIGNUP, null) { r ->
                run {
                    val response = checkNotNull(r) { "Invalid response from sign-up" }

                    val emailAddress = response["username"]?.trim() ?: ""
                    val password = response["password"]?.trim() ?: ""
                    val phone = response["phone"]?.trim() ?: ""
                    val name = response["name"]?.trim() ?: ""

                    check(emailAddress.isNotEmpty()) { "Email Address is empty" }
                    check(password.isNotEmpty()) { "Password is empty" }
                    check(phone.isNotEmpty()) { "Phone is empty" }
                    check(name.isNotEmpty()) { "Name is empty" }
                    val username = toUsername(emailAddress)

                    val userAttributes = CognitoUserAttributes().apply {
                        addAttribute("phone_number", phone)
                        addAttribute("email", emailAddress)
                        addAttribute("name", name)
                    }

                    userPool.signUpInBackground(username, password, userAttributes, null, object : SignUpHandler {
                        /**
                         * This method is called successfully registering a new user.
                         * Confirming the user may be required to activate the users account.
                         *
                         * @param cognitoUser [CognitoUser]
                         * @param signUpConfirmationState will be `true` is the user has been confirmed,
                         * otherwise `false`.
                         * @param cognitoUserCodeDeliveryDetails REQUIRED: Indicates the medium and destination of the confirmation code.
                         */
                        override fun onSuccess(cognitoUser: CognitoUser?, signUpConfirmationState: Boolean, cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails?) {
                            if (signUpConfirmationState) {
                                runOnUiThread {
                                    updateStoredUsername(emailAddress)
                                    handler(IdentityRequest.SUCCESS, null, DO_NOTHING)
                                }
                            } else {
                                val details = checkNotNull(cognitoUserCodeDeliveryDetails) { "Invalid desitnation details" }
                                val user = checkNotNull(cognitoUser) { "Invalid user record" }
                                val parameters = mapOf(
                                        "deliveryVia" to details.deliveryMedium,
                                        "deliveryTo" to details.destination
                                )
                                runOnUiThread {
                                    handler(IdentityRequest.NEED_MULTIFACTORCODE, parameters) { rr ->
                                        run {
                                            val mfaCode = rr?.getOrDefault("mfaCode", "") ?: ""
                                            user.confirmSignUpInBackground(mfaCode, false, object : GenericHandler {
                                                /**
                                                 * This callback method is invoked when the call has successfully completed.
                                                 */
                                                override fun onSuccess() = runOnUiThread {
                                                    updateStoredUsername(emailAddress)
                                                    handler(IdentityRequest.SUCCESS, null, DO_NOTHING)
                                                }

                                                /**
                                                 * This callback method is invoked when call has failed. Probe `exception` for cause.
                                                 *
                                                 * @param exception REQUIRED: Failure details.
                                                 */
                                                override fun onFailure(exception: Exception?) = handleFailure(handler, exception?.localizedMessage)
                                            })
                                        }
                                    }
                                }
                            }
                        }

                        /**
                         * This method is called when user registration has failed.
                         * Probe `exception` for cause of the failure.
                         *
                         * @param exception REQUIRED: Failure details.
                         */
                        override fun onFailure(exception: Exception?) = handleFailure(handler, exception?.localizedMessage)
                    })
                }
            }
        } catch (exception: Exception) {
            handleFailure(handler, exception.localizedMessage)
        }
    }

    /**
     * Initiate a forgot password flow
     */
    override fun initiateForgotPassword(handler: IdentityHandler) {
        try {
            handler(IdentityRequest.NEED_CREDENTIALS, null) { r ->
                run {
                    val response = checkNotNull(r) { "Invalid response when requesting new credentials" }
                    val username = checkNotNull(response["username"]?.trim()) { "Username must be specified" }
                    val password = checkNotNull(response["password"]?.trim()) { "Password must be specified" }

                    check(username.isNotEmpty()) { "Username must be specified" }
                    check(password.isNotEmpty()) { "New password must be specified" }

                    userPool.getUser(toUsername(username)).forgotPasswordInBackground(object : ForgotPasswordHandler {
                        /**
                         * This is called after successfully setting new password for a user.
                         * The new password can new be used to authenticate this user.
                         */
                        override fun onSuccess() = runOnUiThread {
                            handler(IdentityRequest.SUCCESS, null, DO_NOTHING)
                        }

                        /**
                         * This is called for all fatal errors encountered during the password reset process
                         * Probe {@exception} for cause of this failure.
                         * @param exception REQUIRED: Contains failure details.
                         */
                        override fun onFailure(exception: Exception?)  = handleFailure(handler, exception?.localizedMessage)

                        /**
                         * A code may be required to confirm and complete the password reset process
                         * Supply the new password and the confirmation code - which was sent through email/sms
                         * to the continuation
                         * @param forgotPasswordContinuation REQUIRED: Continuation to the next step.
                         */
                        override fun getResetCode(forgotPasswordContinuation: ForgotPasswordContinuation) = runOnUiThread {
                            val continuation = checkNotNull(forgotPasswordContinuation) { "Invalid continuation" }
                            val delivery = checkNotNull(continuation.parameters) { "Invalid continuation token" }
                            val parameters = mapOf(
                                "deliveryVia" to delivery.deliveryMedium,
                                "deliveryTo" to delivery.destination
                            )
                            handler(IdentityRequest.NEED_MULTIFACTORCODE, parameters) { r ->
                                run {
                                    val mfa = checkNotNull(r) { "Invalid MFA response" }
                                    with (continuation) {
                                        setPassword(response["password"]?.trim() ?: "")
                                        setVerificationCode(mfa["mfaCode"]?.trim() ?: "")
                                        continueTask()
                                    }
                                }
                            }
                        }
                    })
                }
            }
        } catch (exception : Exception) {
            handleFailure(handler, exception.localizedMessage)
        }
    }

    /**
     * Initiate a sign-out flow
     */
    override fun signOut() {
        userPool.currentUser.signOut()
        mCurrentUser.postValue(null)
    }
}