package com.amazonaws.mobile.samples.chatql.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.amazonaws.mobile.samples.chatql.R
import com.amazonaws.mobile.samples.chatql.ui.auth.SignInActivity
import kotlinx.android.synthetic.main.activity_splash.*
import org.jetbrains.anko.startActivity
import kotlin.concurrent.thread

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        splash_progress.isIndeterminate = true
    }

    override fun onResume() {
        super.onResume()

        // Wait for five seconds, spinning the icon, then transition
        thread(start = true) {
            try {
                Thread.sleep(5000L)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } finally {
                startActivity<SignInActivity>()
            }
        }
    }
}
