package com.amazonaws.mobile.samples.chatql.models

import com.amazonaws.mobile.samples.chatql.models.TokenType
import java.util.*

/**
 * Stores information about the currently authenticated user
 */
class AuthUser(val id: String = UUID.randomUUID().toString(), var username: String = "") {
    val userAttributes: MutableMap<String, String> = HashMap()
    val tokens: MutableMap<TokenType, String> = HashMap()
}