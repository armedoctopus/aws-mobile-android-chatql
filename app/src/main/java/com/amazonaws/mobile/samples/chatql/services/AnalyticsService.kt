package com.amazonaws.mobile.samples.chatql.services

enum class AuthenticationType {
    SIGN_IN,
    SIGN_UP,
    PASSWORD_RESET
}

interface AnalyticsService {
    /**
     * Record a custom event into the analytics stream
     *
     * @param name the custom event name
     * @param [attributes] a list of key-value pairs for recording string attributes
     * @param [metrics] a list of key-value pairs for recording numeric metrics
     */
    fun recordEvent(name: String, attributes: Map<String,String>? = null, metrics: Map<String,Double>? = null)

    /**
     * Record an authentication record
     *
     * @param authType the authentication type
     * @param isSuccessful true if the authentication was successful
     * @param username the username that was used
     */
    fun recordAuthenticationEvent(authType: AuthenticationType, isSuccessful: Boolean, username: String)
}