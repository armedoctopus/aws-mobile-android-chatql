package com.amazonaws.mobile.samples.chatql.models

/**
 * The TokenType is a list of allowable tokens
 */
enum class TokenType {
    ID_TOKEN,
    ACCESS_TOKEN,
    REFRESH_TOKEN
}