service: chatql

plugins:
  modules:
    - serverless-cf-vars
    - serverless-appsync-plugin
    - aws-amplify-serverless-plugin

provider:
  name                                    : aws
  region                                  : us-east-1
  runtime                                 : nodejs8.10
  stage                                   : ${opt:stage, 'dev'}
  apiname                                 : ${opt:apiname, 'chatql'}_${self:provider.stage}
  environment:
    GRAPHQL_API                           : { Fn::GetAtt: [ GraphQlApi, GraphQLUrl ] }
    REGION                                : ${self:provider.region}

functions:
  postAuthentication:
    handler                               : cognito.postauth
    role                                  : PostAuthenticationRole
    events:
      - cognitoUserPool:
          pool                            : MyPool
          trigger                         : PostAuthentication

custom:
  region                                  : ${opt:region, self:provider.region}

  amplify:
    - filename                            : ../app/src/main/res/raw/awsconfiguration.json
      type                                : native
      appClient                           : AndroidUserPoolClient

  appSync:
    name                                  : ${self:provider.apiname}
    region                                : ${self:provider.region}
    authenticationType                    : AWS_IAM
    serviceRole                           : "AppSyncServiceRole"
    dataSources:
      - type                              : AMAZON_DYNAMODB
        name                              : "UsersTable"
        description                       : "DynamoDB Users Table"
        config:
          tableName                       : { Ref: UsersTable }
          iamRoleStatements:
            - Effect                      : Allow
              Action:
                - "dynamodb:Query"
                - "dynamodb:PutItem"
              Resource:
                - { Fn::GetAtt: [ UsersTable, Arn ]}
                - { Fn::Join: [ '', [ { Fn::GetAtt: [ UsersTable, Arn ] }, '/*' ] ] }

    schema                                : schema.graphql
    mappingTemplates:
      - type                              : Query
        field                             : allUsers
        dataSource                        : "UsersTable"
        request                           : "request-allUsers.vtl"
        response                          : "response-UserPagedConnection.vtl"

      - type                              : Mutation
        field                             : addUser
        dataSource                        : "UsersTable"
        request                           : "request-addUser.vtl"
        response                          : "response-single.vtl"

      - type                              : Mutation
        field                             : updateUser
        dataSource                        : "UsersTable"
        request                           : "request-updateUser.vtl"
        response                          : "response-single.vtl"

resources:
  Resources:
    #=======================================================================================
    # Note: This logicalID must start with "CognitoUserPool" because that specific string
    # is used by the functions section to link the PostAuthentication trigger to it.
    CognitoUserPoolMyPool:
      Type                                : AWS::Cognito::UserPool
      Description                         : "Username / Password auth database"
      Properties:
        UserPoolName                      : ${self:provider.apiname}
        Schema:
          - Name                          : email
            Required                      : true
            Mutable                       : true
        Policies:
          PasswordPolicy:
            MinimumLength                 : 6
            RequireLowercase              : false
            RequireUppercase              : false
            RequireNumbers                : false
            RequireSymbols                : false
        AutoVerifiedAttributes            : [ "email" ]
        MfaConfiguration                  : "OFF"
    #=======================================================================================
    AndroidUserPoolClient:
      Type                                : AWS::Cognito::UserPoolClient
      Description                         : "OAuth2 app client for Android app"
      Properties:
        ClientName                        : ${self:provider.apiname}-android
        GenerateSecret                    : true
        UserPoolId                        : { Ref: CognitoUserPoolMyPool }
    #=======================================================================================
    #=======================================================================================
    IdentityPool:
      Type                                : AWS::Cognito::IdentityPool
      Description                         : "Federation for the User Pool members to access AWS resources"
      Properties:
        IdentityPoolName                  : ${self:provider.apiname}_identities
        AllowUnauthenticatedIdentities    : true
        CognitoIdentityProviders:
          - ClientId                      : { Ref: AndroidUserPoolClient }
            ProviderName                  : { Fn::Sub: [ 'cognito-idp.${self:provider.region}.amazonaws.com/#{client}', { "client": { Ref: CognitoUserPoolMyPool }}]}
    #=======================================================================================
    AuthRole:
      Type                                : AWS::IAM::Role
      Description                         : "Role that the an authenticated user assumes"
      Properties:
        RoleName                          : ${self:provider.apiname}-auth
        AssumeRolePolicyDocument:
          Version                         : "2012-10-17"
          Statement:
            - Effect                      : Allow
              Principal:
                Federated                 : cognito-identity.amazonaws.com
              Action                      : sts:AssumeRoleWithWebIdentity
              Condition:
                ForAnyValue:StringLike:
                  "cognito-identity.amazon.com:amr": "authenticated"
        Policies:
          - PolicyName                    : ${self:provider.apiname}-auth-appsync
            PolicyDocument:
              Version                     : "2012-10-17"
              Statement:
                - Effect                  : Allow
                  Action                  : appsync:GraphQL
                  Resource:
                    - { Fn::Join: [ '', [ { Ref: GraphQlApi }, '/types/Query/fields/*' ] ] }
                    - { Fn::Join: [ '', [ { Ref: GraphQlApi }, '/types/Subscription/fields/*' ] ] }
                    - { Fn::Join: [ '', [ { Ref: GraphQlApi }, '/types/Mutation/fields/updateUser' ] ] }
    #=======================================================================================
    UnAuthRole:
      Type                                : AWS::IAM::Role
      Description                         : "Role that the an authenticated user assumes"
      Properties:
        RoleName                          : ${self:provider.apiname}-unauth
        AssumeRolePolicyDocument:
          Version                         : "2012-10-17"
          Statement:
            - Effect                      : Allow
              Principal:
                Federated                 : cognito-identity.amazonaws.com
              Action                      : sts:AssumeRoleWithWebIdentity
              Condition:
                ForAnyValue:StringLike:
                  "cognito-identity.amazon.com:amr": "unauthenticated"
    #=======================================================================================
    IdentityPoolRoleMap:
      Type                                : AWS::Cognito::IdentityPoolRoleAttachment
      Description                         : "Links the unauthenticated and authenticated policies to the identity pool"
      Properties:
        IdentityPoolId                    : { Ref: IdentityPool }
        Roles:
          unauthenticated                 : { Fn::GetAtt: [ UnAuthRole, Arn ]}
          authenticated                   : { Fn::GetAtt: [ AuthRole, Arn ]}
    #=======================================================================================
    PostAuthenticationRole:
      Type                                : AWS::IAM::Role
      Properties:
        RoleName                          : ${self:provider.apiname}-postauth-lambda
        AssumeRolePolicyDocument:
          Version                         : "2012-10-17"
          Statement:
            - Effect                      : Allow
              Principal:
                Service                   : lambda.amazonaws.com
              Action                      : sts:AssumeRole
        Policies:
          - PolicyName                    : ${self:provider.apiname}-postauth-appsync
            PolicyDocument:
              Version                     : "2012-10-17"
              Statement:
                - Effect                  : Allow
                  Action                  : appsync:GraphQL
                  Resource                : { Fn::Join: [ '', [ { Ref: GraphQlApi }, '/types/Mutation/fields/addUser' ] ] }
          - PolicyName                    : ${self:provider.apiname}-postauth-cloudwatch
            PolicyDocument:
              Version                     : "2012-10-17"
              Statement:
                - Effect                  : Allow
                  Action                  : logs:CreateLogGroup
                  Resource                : "arn:aws:logs:${self:provider.region}:#{AWS::AccountId}:*"
                - Effect                  : Allow
                  Action                  : [ logs:CreateLogStream, logs:PutLogEvents ]
                  Resource                : "arn:aws:logs:${self:provider.region}:#{AWS::AccountId}:log-group:/aws/lambda/${self:service}-${self:provider.stage}-postAuthentication:*"
    #=======================================================================================
    #=======================================================================================
    UsersTable:
      Type                                : AWS::DynamoDB::Table
      Description                         : "Storage for the users list"
      Properties:
        TableName                         : ${self:provider.apiname}-users
        AttributeDefinitions:
          - AttributeName                 : userId
            AttributeType                 : S
        KeySchema:
          - AttributeName                 : userId
            KeyType                       : HASH
        ProvisionedThroughput:
          ReadCapacityUnits               : 5
          WriteCapacityUnits              : 5
    #=======================================================================================